
import express from 'express';
import cors from 'cors';
import { MissingResourceError, UnableToExecuteError } from './errors';

import { Expenses, Weddings} from './entities';
import { WeddingsPlannerServiceImpl } from './services/wedding-service-impl';
import WeddingsPlannerService from './services/wedding-service';
import ExpensesService from './services/expense-service';
import { ExpensesServiceImpl } from './services/expenses-service-impl';

const app = express();
app.use(express.json());
app.use(cors());


const weddingsPlannerService: WeddingsPlannerService = new WeddingsPlannerServiceImpl();
const expensesService: ExpensesService = new ExpensesServiceImpl();

// get all weddings
app.get("/weddings", async(req,res)=> {
    console.log("getting all weddings \n");
    const weddings:Weddings[] = await weddingsPlannerService.getAllWeddings();
    res.send(weddings);
});


// create a wedding
app.post("/weddings", async(req, res) => {
    console.log("creating a wedding");
    let weddingObj:Weddings = req.body;
    weddingObj  = await weddingsPlannerService.createWedding(weddingObj);
    res.send(weddingObj);
});


// get wedding by id
app.get("/weddings/:id", async(req, res) => {
    try{
        const wedding_id = Number(req.params.id);
        const weddingObj:Weddings = await weddingsPlannerService.getWeddingById(wedding_id);
        res.send(weddingObj);
    }

    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});





app.delete("/weddings/:id", async(req,res) => {
    try{
        console.log("deleting wedding by id");
        const wedding_id = Number(req.params.id);
        //check if client exists first 
        const wedding: Weddings = await weddingsPlannerService.getWeddingById(wedding_id);

        //if no misssing resource then we can go ahead and call the api 
        const result = await weddingsPlannerService.deleteWeddingById(wedding.wedding_id);
        
        res.status(201); // upon success we return 201
        res.send(result); // return of the API
    }

    catch (error) {
        if(error instanceof MissingResourceError){
            
            res.status(404);
            res.send(error);
            
        }
    }
})

app.put("/weddings/:id", async(req,res) => {
    try{
        const wedding_id = Number(req.params.id);
        let weddingObj:Weddings = req.body;

        let retrievedWedding:Weddings = await weddingsPlannerService.getWeddingById(wedding_id);

        retrievedWedding.wedding_name = weddingObj.wedding_name? weddingObj.wedding_name : retrievedWedding.wedding_name;
        retrievedWedding.end_time = weddingObj.end_time? weddingObj.end_time : retrievedWedding.end_time;
        retrievedWedding.start_time = weddingObj.start_time ? weddingObj.start_time : retrievedWedding.start_time;
        retrievedWedding.groom = weddingObj.groom? weddingObj.groom: retrievedWedding.groom;
        retrievedWedding.bride = weddingObj.bride? weddingObj.bride: retrievedWedding.bride;
        retrievedWedding.venue_id = weddingObj.venue_id? weddingObj.venue_id : retrievedWedding.venue_id;
        retrievedWedding.wedding_description = weddingObj.wedding_description ? weddingObj.wedding_description : retrievedWedding.wedding_description;
        retrievedWedding.no_of_guests = weddingObj.no_of_guests ? weddingObj.no_of_guests : retrievedWedding.no_of_guests;
        retrievedWedding.budget_planned = weddingObj.budget_planned;
        //send the object that was sent by 
        const result:Weddings = await weddingsPlannerService.updateWedding(retrievedWedding);
        console.log(result);

        //after replacing check if created and can be found
        const updatedWedding: Weddings = await weddingsPlannerService.getWeddingById(result.wedding_id);

        res.send(updatedWedding);
    }

    catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

app.get("/expenses", async(req,res)=> {
    try{
        console.log("getting all expenses \n");
        const expenses:Expenses[] = await expensesService.getAllExpenses();
        res.send(expenses);
    }
    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }

        if(error instanceof UnableToExecuteError){
            res.status(404);
            res.send(error);
        }
    }
});

app.post("/expenses", async(req,res)=> {
    console.log("Posting an expenses \n");
    let expenseBody:Expenses = req.body;
    const expense:Expenses = await expensesService.createExpense(expenseBody);
    res.send(expense);

});


app.get("/expenses/:id", async(req,res)=> {
    try {
        console.log("getting a expense by id \n");
        const expense_id = Number(req.params.id);
        const expense: Expenses = await expensesService.getExpensesById(expense_id);
        res.send(expense);
    }
    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }

        if(error instanceof UnableToExecuteError){
            res.status(404);
            res.send(error);
        }
    }
    
});

app.delete("/expenses/:id", async(req,res)=> {
    console.log("deleting a expense by id \n");
    try {
        const expense_id = Number(req.params.id);
        const expenseDeleted: Boolean = await expensesService.deleteExpenseById(expense_id);
        res.send(expenseDeleted);
    }
    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }

        if(error instanceof UnableToExecuteError){
            res.status(404);
            res.send(error);
        }
    }
    
});

app.put("/expenses/:id", async(req,res)=> {
    console.log("updating a expense by id \n");
    try {
        const expense_id = Number(req.params.id);

        await expensesService.getExpensesById(expense_id);

        let expenseBody:Expenses = req.body;
        
        const expenseUpdated: Expenses = await expensesService.updateExpenses(expenseBody);
        console.log("updated expense in index.ts: "+ expenseUpdated)
        res.send(expenseUpdated);
    }
    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }

        if(error instanceof UnableToExecuteError){
            res.status(404);
            res.send(error);
        }
    }
    
});

app.get("/weddings/:id/expenses", async(req,res)=> {
    try {
        
    
        console.log("getting a wedding expense by wedding_id \n");
        const wedding_id = Number(req.params.id);
        const expenses:Expenses[] = await weddingsPlannerService.getWeddingExpenseById(wedding_id);
        console.log("after getting wedding");
        res.send(expenses);
        
    }
    catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }

        if(error instanceof UnableToExecuteError){
            res.status(404);
            res.send(error);
        }
    }
    
});



app.listen(3001, ()=> {console.log("Application Started ASH")});
