

export class Expenses {
    constructor (public expense_id: number,
        public reason: string,
        public wedding_id: number,
        public amount: number){

    }
}




export class Weddings {
    constructor(
        public wedding_id: number,
        public wedding_name: string,
        public wedding_description: string,
        public venue_id: string,     //foreign key
        public groom: string, //user_id- foreign key - from Users table
        public bride: string,
        public start_time: string,
        public end_time: string, 
        public budget_planned: number,
        public no_of_guests: number)
    {
        
    }
}

