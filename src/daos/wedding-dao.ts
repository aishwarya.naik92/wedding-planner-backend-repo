/* 
File: Wedding DAO interface declaration
*/
import { Expenses} from "../entities";
import { Weddings } from "../entities";

export interface WeddingsDAO{

    getAllWeddings(): Promise<Weddings[]>;

    //create a wedding - POST
    createWedding(weddingObj: Weddings): Promise<Weddings>;

    //delete a wedding - DELETE
    deleteWeddingById(wedding_id: number): Promise<Boolean>;

    //get a wedding by id
    getWeddingById(wedding_id: number): Promise<Weddings>;

    //get wedding expense by passing wedding id
    getWeddingExpenseById(wedding_id: number): Promise<Expenses[]>;

    // put request - update wedding 
    updateWedding(weddingObj: Weddings) : Promise<Weddings>;

}