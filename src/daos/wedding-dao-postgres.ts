import { client } from "../connection";
import { Expenses, Weddings } from "../entities";
import { MissingResourceError, UnableToExecuteError } from "../errors";
import { WeddingsDAO } from "./wedding-dao";


export class WeddingPostgres implements WeddingsDAO{
    async getAllWeddings(): Promise<Weddings[]> {
        const sql: string = "select * from weddings";
        const result = await client.query(sql);

        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The weddings do not exist`);
        }
        const weddings:Weddings[] = [];

        for(const line of result.rows)
        {
            console.log(line);
            const weddingObj: Weddings = new Weddings(
                line.wedding_id,
                line.wedding_name,
                line.wedding_description, 
                line.venue_id,
                line.groom,
                line.bride,
                line.start_time,
                line.end_time,
                line.budget_planned,
                line.no_of_guests
            );
            weddings.push(weddingObj);
        }

        return weddings;
    }

    async getWeddingExpenseById(wedding_id: number): Promise<Expenses[]> {

        const sql:string = "select * from expenses where wedding_id=$1";
        const values = [wedding_id];
        const result = await client.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense for wedding with id ${wedding_id} does not exist`);
        }
        
        return result.rows;
    }

    async updateWedding(weddingObj: Weddings): Promise<Weddings> {
        const sql:string = "update weddings set wedding_name=$1, wedding_description=$2, venue_id=$3, groom=$4, bride=$5, start_time=$6, end_time=$7, budget_planned=$8, no_of_guests=$9 where wedding_id=$10";
        const values = [weddingObj.wedding_name, 
            weddingObj.wedding_description, 
            weddingObj.venue_id,
            weddingObj.groom,
            weddingObj.bride,
            weddingObj.start_time, 
            weddingObj.end_time,
            weddingObj.budget_planned,
            weddingObj.no_of_guests,
        weddingObj.wedding_id];

        const result = await client.query(sql, values);
        console.log("update: "+ sql);
        console.log("values");
        console.log(result);

        if(result.rowCount === 0)
        {
            throw new UnableToExecuteError(`The wedding with id ${weddingObj.wedding_id} could not be created`);
        }

        return weddingObj;

    }
    
    async createWedding(weddingObj: Weddings): Promise<Weddings>
    {
        const sql:string = "insert into Weddings(wedding_name, wedding_description, venue_id, groom, bride,  start_time, end_time, budget_planned, no_of_guests) values ($1, $2, $3, $4, $5, $6, $7, $8, $9) returning wedding_id";
        const values = [weddingObj.wedding_name, 
            weddingObj.wedding_description, 
            weddingObj.venue_id, 
            weddingObj.groom, 
            weddingObj.bride,
            weddingObj.start_time, 
            weddingObj.end_time, 
            weddingObj.budget_planned,
            weddingObj.no_of_guests];
            
        const result = await client.query(sql, values);

        if(result.rowCount === 0)
        {
            throw new UnableToExecuteError(`The wedding with id ${weddingObj.wedding_id} could not be created`);
        }

        weddingObj.wedding_id = result.rows[0].wedding_id;
        return weddingObj;
    }

    async deleteWeddingById(wedding_id: number): Promise<Boolean> {
        
        const sql1:string = "delete from Expenses where wedding_id=$1";
        const values = [wedding_id];
        const sql2:string = "delete from Weddings where wedding_id=$1";
        
        await client.query(sql1, values);

        const result2 = await client.query(sql2, values);
        if(result2.rowCount === 0)
        {
            throw new MissingResourceError(`The wedding with id ${wedding_id} does not exist`);
        }

        return true;
    }

    async getWeddingById(wedding_id: number): Promise<Weddings> {
        const sql:string = "select * from weddings where wedding_id=$1";
        const values = [wedding_id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${wedding_id} does not exist`);
        }
        const row = result.rows[0];

        const wedding:Weddings = new Weddings(
            row.wedding_id,
            row.wedding_name,
            row.wedding_description,
            row.venue_id,
            row.groom,
            row.bride,
            row.start_time,
            row.end_time,
            row.budget_planned,
            row.no_of_guests);

        return wedding ;
    }




    // async getWeddingExpenseById(wedding_id: number): Promise<Expense> {
    //     throw new Error("Method not implemented.");
    // }
}