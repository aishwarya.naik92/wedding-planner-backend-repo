import { Expenses } from "../entities";

export interface ExpenseDAO  {

    getExpensesById(expense_id: number) : Promise<Expenses>;

    getAllExpenses(): Promise<Expenses[]>;

    createExpenses(expenseObj: Expenses) : Promise<Expenses>;

    putExpensesById(expenseObj: Expenses) : Promise<Expenses>;

    deleteExpensesById(expense_id: number) : Promise<Boolean>; 
}