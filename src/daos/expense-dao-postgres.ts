import { client } from "../connection";
import { Expenses } from "../entities";
import { MissingResourceError } from "../errors";
import { ExpenseDAO } from "./expense-dao";

export class ExpenseDAOPostgres implements ExpenseDAO {

    async getExpensesById(expense_id: number): Promise<Expenses> {
        const sql:string = "select * from expenses where expense_id=$1";
        const values = [expense_id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expense_id} does not exist`);
        }

        const row = result.rows[0];

        const expense:Expenses = new Expenses(
            row.expense_id,
            row.reason,
            row.wedding_id,
            row.amount
        );

        return expense;
    }


    async getAllExpenses(): Promise<Expenses[]> {
        const sql: string = "select * from expenses";
        const result = await client.query(sql);
        if(result.rowCount === 0){
            throw new MissingResourceError(`Expenses does not exist`);
        }

        const expenses:Expenses[] = [];
        for(const line of result.rows)
        {
            const expensesObj :Expenses = new Expenses(
                line.expense_id,
                line.reason,
                line.wedding_id,
                line.amount
            )
            expenses.push(expensesObj);
        }
        return expenses;

    }
    async createExpenses(expenseObj: Expenses): Promise<Expenses> {
        console.log (expenseObj);
        const sql:string = "insert into Expenses( reason, wedding_id, amount) values ($1, $2, $3) returning expense_id";
        const values = [expenseObj.reason, expenseObj.wedding_id, expenseObj.amount];
        const result = await client.query(sql, values);
        expenseObj.expense_id = result.rows[0].expense_id;
        return expenseObj;

    }
    async putExpensesById(expenseObj: Expenses): Promise<Expenses> {
        const sql: string = "update expenses set reason=$1, wedding_id=$2, amount=$3 where expense_id=$4 returning *";
        const values = [expenseObj.reason, expenseObj.wedding_id, expenseObj.amount,  expenseObj.expense_id];

        const result = await client.query(sql,values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`Could not update expense with id ${expenseObj.expense_id}`);
        }

        const row = result.rows[0];
        console.log(result.rows[0]);
        const expensesObj :Expenses = new Expenses(
            row.expense_id,
            row.reason,
            row.wedding_id,
            row.amount
        );
        console.log(expensesObj);
        return  expensesObj;
    }

    async deleteExpensesById(expense_id: number): Promise<Boolean> {
        const sql: string = "delete from expenses where expense_id=$1";
        const values = [expense_id];

        await client.query(sql, values);
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The expense with id ${expense_id} does not exist`);
        }

        return true;
        
    }
    
}