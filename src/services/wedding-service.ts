import {Expenses, Weddings} from "../entities";

export default interface WeddingsPlannerService{
    
    createWedding(weddingObj: Weddings): Promise<Weddings>;

    getAllWeddings(): Promise<Weddings[]>;

    getWeddingById(wedding_id: number) : Promise <Weddings>;

    deleteWeddingById(wedding_id: number): Promise<Boolean>;

    updateWedding(weddingObj: Weddings): Promise<Weddings>;

    getWeddingExpenseById(wedding_id: Number): Promise<Expenses[]>;

}