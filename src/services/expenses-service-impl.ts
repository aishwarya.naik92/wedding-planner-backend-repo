import { ExpenseDAOPostgres } from "../daos/expense-dao-postgres";
import { Expenses } from "../entities";
import ExpensesService from "./expense-service";

export class ExpensesServiceImpl implements ExpensesService{

    expensesDAO:ExpenseDAOPostgres = new ExpenseDAOPostgres();
    createExpense(expenseObj: Expenses): Promise<Expenses> {
        return this.expensesDAO.createExpenses(expenseObj);
    }
    getAllExpenses(): Promise<Expenses[]> {
        return this.expensesDAO.getAllExpenses();
    }
    getExpensesById(expense_id: number): Promise<Expenses> {
        return this.expensesDAO.getExpensesById(expense_id);
    }
    deleteExpenseById(expense_id: number): Promise<Boolean> {
        return this.expensesDAO.deleteExpensesById(expense_id);
    }
    updateExpenses(expenseObj: Expenses): Promise<Expenses> {
        return this.expensesDAO.putExpensesById(expenseObj);
    }
    
}