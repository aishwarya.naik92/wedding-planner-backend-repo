import { WeddingPostgres } from "../daos/wedding-dao-postgres";
import { Expenses, Weddings } from "../entities";
import WeddingsPlannerService from "./wedding-service";

export class WeddingsPlannerServiceImpl implements WeddingsPlannerService{
    

    weddingDAO:WeddingPostgres = new WeddingPostgres();

    createWedding(weddingObj: Weddings): Promise<Weddings> {
        return this.weddingDAO.createWedding(weddingObj);
    }
    getAllWeddings(): Promise<Weddings[]> {
        return this.weddingDAO.getAllWeddings();
    }
    getWeddingById(wedding_id: number): Promise<Weddings> {
        return this.weddingDAO.getWeddingById(wedding_id);
    }
    deleteWeddingById(wedding_id: number): Promise<Boolean> {
        return this.weddingDAO.deleteWeddingById(wedding_id);
    }

    updateWedding(weddingObj: Weddings): Promise<Weddings> {
        return this.weddingDAO.updateWedding(weddingObj);
    }

    getWeddingExpenseById(wedding_id: number): Promise<Expenses[]>{
        return this.weddingDAO.getWeddingExpenseById(wedding_id);
    }

}