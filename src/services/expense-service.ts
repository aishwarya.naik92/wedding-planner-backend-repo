import {Expenses} from "../entities";

export default interface ExpensesService{
    
    createExpense(expenseObj: Expenses): Promise<Expenses>;

    getAllExpenses(): Promise<Expenses[]>;

    getExpensesById(expense_id: number) : Promise <Expenses>;

    deleteExpenseById(expense_id: number): Promise<Boolean>;

    updateExpenses(expenseObj: Expenses): Promise<Expenses>;
}