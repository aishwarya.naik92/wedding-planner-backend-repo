import { getDefaultCompilerOptions } from "typescript";
import { client } from "../src/connection";
import { WeddingsDAO } from "../src/daos/wedding-dao";
import { WeddingPostgres } from "../src/daos/wedding-dao-postgres";
import { ExpenseDAOPostgres } from "../src/daos/expense-dao-postgres";
import { Expenses, Weddings } from "../src/entities";
import { ExpenseDAO } from "../src/daos/expense-dao";

const weddingsDAO: WeddingsDAO = new WeddingPostgres();
const expensesDAO: ExpenseDAO = new ExpenseDAOPostgres();

// test("-----TC 1: CREATE/POST Weddings for a Client-----", async()=>{
//     const bride = 1;
//     const groom = 2;
    
    
//     let weddingsEntry:Weddings = {
//         wedding_id: 0,
//         wedding_name: 'TestAccount',
//         wedding_description: 'N',
//         venue_id: "Leela Bangalore",
//         groom: "Create Test Groom",
//         bride: "Create Test Bride",
//         start_time: '10:00 am',
//         end_time: '11:00 am',
//         budget_planned: 30000,
//         no_of_guests: 100
//     } ;

//     let weddingObj:Weddings = new Weddings(weddingsEntry.wedding_id,
//         weddingsEntry.wedding_name, 
//         weddingsEntry.wedding_description,
//         weddingsEntry.venue_id,
//         weddingsEntry.bride,
//         weddingsEntry.groom,
//         weddingsEntry.start_time,
//         weddingsEntry.end_time,
//         weddingsEntry.budget_planned,
//         weddingsEntry.no_of_guests
//     );

//     // persist wedding obj in database
//     weddingObj = await weddingsDAO.createWedding(weddingObj); 

//     // get the weddong from database once created 
//     const retrievedWedding = await weddingsDAO.getWeddingById(weddingObj.wedding_id);

//     expect(retrievedWedding.wedding_id).toBe(1);
// });


// test("-----TC 2: READ/GET all Weddings -----", async()=>{
//     const allWeddings:Weddings[] = await weddingsDAO.getAllWeddings();
//     expect(allWeddings.length).toBeGreaterThanOrEqual(1);
// });


// test("-----TC 3: READ/GET Wedding by weddingId-----", async()=> {
//     const bride = 4;
//     const groom = 2;
    

//     let weddingsEntry:Weddings = {
//         wedding_id: 0,
//         wedding_name: 'Test Wedding',
//         wedding_description: 'test wedding description',
//         venue_id: "Hilton, Goa",
//         groom: "Read Test Groom",
//         bride: "Read Test Bride",
//         start_time: '9:00 pm',
//         end_time: '1:30 pm',
//         budget_planned: 20000,
//         no_of_guests: 290
//     } ;

//     let weddingObj:Weddings = new Weddings(weddingsEntry.wedding_id,
//         weddingsEntry.wedding_name, 
//         weddingsEntry.wedding_description,
//         weddingsEntry.venue_id,
//         weddingsEntry.bride,
//         weddingsEntry.groom,
//         weddingsEntry.start_time,
//         weddingsEntry.end_time,
//         weddingsEntry.budget_planned,
//         weddingsEntry.no_of_guests
//     );

//     // persist client obj in database
//     weddingObj = await weddingsDAO.createWedding(weddingObj); 
    
//     // get the weddong from database once created 
//     const retrievedWedding = await weddingsDAO.getWeddingById(weddingObj.wedding_id);    
//     expect(retrievedWedding.wedding_id).toBe(2);
// });

// test("-----TC 4: DELETE Wedding by wedding_id-----", async()=> {
    
//     const venue=4;
    
//     let weddingsEntry:Weddings = {
//         wedding_id: 0,
//         wedding_name: 'Second - Test Wedding 2',
//         wedding_description: '2 test wedding description',
//         venue_id: 'Taj, Bombay',
//         groom: "Delete Test Groom",
//         bride: "Delete Test Bride",
//         start_time: '9:08 am',
//         end_time: '1:30 pm',
//         budget_planned: 10000,
//         no_of_guests: 50
//     } ;

//     let weddingObj:Weddings = new Weddings(weddingsEntry.wedding_id,
//         weddingsEntry.wedding_name, 
//         weddingsEntry.wedding_description,
//         weddingsEntry.venue_id,
//         weddingsEntry.bride,
//         weddingsEntry.groom,
//         weddingsEntry.start_time,
//         weddingsEntry.end_time,
//         weddingsEntry.budget_planned,
//         weddingsEntry.no_of_guests
//     );

//     // persist client obj in database
//     weddingObj = await weddingsDAO.createWedding(weddingObj); 
//     const retrievedWedding = await weddingsDAO.getWeddingById(weddingObj.wedding_id);
    

//     const weddingDeleted:Boolean = await weddingsDAO.deleteWeddingById(retrievedWedding.wedding_id);
//     console.log("------Ending  DELETE Wedding BY ID------");
//     expect(weddingDeleted).toBeTruthy();
// });


// test("-----TC 5: Update Wedding by Wedding ID-----", async() => {
//     console.log("------Attempting to Update Wedding BY ID------");

//     let retrievedWedding = await weddingsDAO.getWeddingById(1);
//     retrievedWedding.wedding_name = "Updating Wedding Name";
//     retrievedWedding.wedding_description = "This is an update request";
//     retrievedWedding.no_of_guests = 10;
    
//     const wedding:Weddings = await weddingsDAO.updateWedding(retrievedWedding);

//     const verifiedWeddingObj = await weddingsDAO.getWeddingById(wedding.wedding_id);

//     console.log("------Ending UPDATE Expenses BY ID------")
//     expect(verifiedWeddingObj.no_of_guests).toEqual(10);

// });

// test("--------TC 7: Create an Expense-----", async() => {
//     console.log("------Creating an expense--------");

//     let expenseObj:Expenses = new Expenses(0, "Food" , 1, 32000)
//     const expenseCreated:Expenses = await expensesDAO.createExpenses(expenseObj);
//     expect(expenseCreated.expense_id).toBe(1);
// });

// test("------TC 6: Get all Expenses -----", async()=> {
//     console.log("------Get all expenses------");

//     let allExpenses:Expenses[] = await expensesDAO.getAllExpenses();
//     expect(allExpenses.length).toBe(1);

// });

// test("------TC 8: Get Expenses by ExpenseId-----", async()=> {
//     console.log("------Get Expense by ExpenseId------");

//     let expenses:Expenses = await expensesDAO.getExpensesById(1);
//     expect(expenses.amount).toBe(32000);

// });


test("-------TC 9: GET Wedding expense by WeddingId----", async()=> {
    console.log("------Attempting to Update Wedding expense BY wedding ID------");
    let expenses:Expenses[]= await weddingsDAO.getWeddingExpenseById(1);
    console.log(expenses)
    expect(expenses[0].amount).toBe(32000);
})



test("-----TC 10: Update Expense by expense-----", async() => {
    console.log("------Attempting to Update ACCOUNT BY ID------");

    let retrievedExpense:Expenses = await expensesDAO.getExpensesById(1);
    console.log(retrievedExpense);
    let actual = retrievedExpense.amount;   
    retrievedExpense.amount = retrievedExpense.amount + 1000;
    let result = actual +1000;
    const expenses:Expenses = await expensesDAO.putExpensesById(retrievedExpense);
    console.log("after updating: "+ expenses);
    const verfifiedExpenseObj = await expensesDAO.getExpensesById(expenses.expense_id);

    console.log("------Ending UPDATE Expenses BY ID------")
    expect(verfifiedExpenseObj.amount).toEqual(result);

});

afterAll(async()=>{
    client.end()// should close our connection once all tests is over
})
